#filter emptyLines

# LOCALIZATION NOTE: The 'en-US' strings in some URLs will be replaced with
# your locale code, and link to your translated pages as soon as they're live.

#define bookmarks_title Marcadores
#define bookmarks_heading Marcadores

#define personal_toolbarfolder Carpeta de Barra de herramientas personal

#define seamonkey_and_support Recursos de SeaMonkey

# LOCALIZATION NOTE (seamonkey):
# link title for https://www.seamonkey-project.org/ (in the personal toolbar)
#define seamonkey SeaMonkey

# LOCALIZATION NOTE (seamonkey_long):
# link title for https://www.seamonkey-project.org/ (in normal bookmarks)
#define seamonkey_long El Proyecto SeaMonkey

#define extend_seamonkey Extendiendo SeaMonkey

# LOCALIZATION NOTE (seamonkey_addons):
# link title for https://addons.thunderbird.net/en-US/seamonkey/
#define seamonkey_addons Complementos de SeaMonkey

# LOCALIZATION NOTE (seamonkey_themes):
# link title for https://addons.thunderbird.net/en-US/seamonkey/themes
#define seamonkey_themes Temas de SeaMonkey

# LOCALIZATION NOTE (seamonkey_dictionaries):
# link title for https://addons.thunderbird.net/en-US/seamonkey/dictionaries
#define seamonkey_dictionaries Diccionarios ortográficos

#define community_support Comunid&ad y soporte

# LOCALIZATION NOTE (seamonkey_community):
# link title for https://www.seamonkey-project.org/community
#define seamonkey_community Comunidad de SeaMonkey

# LOCALIZATION NOTE (mozillazine):
# link title for http://www.mozillazine.org/
#define mozillazine mozillaZine

# LOCALIZATION NOTE (seamonkey_support):
# link title for the mozillaZine SeaMonkey Support forum
#define seamonkey_support Foro de soporte de SeaMonkey (mozillaZine)

# LOCALIZATION NOTE (seamonkey_l10n):
# insert full bookmark line for localized SeaMonkey page (personal toolbar)
# e.g. #define seamonkey_l10n <DT><A HREF="https://www.seamonkey.tlh/">SeaMonkey tlhIngan</a>
# Do not remove the trailing spaces here or MERGE_FILE in l0n builds will fail!
#define seamonkey_l10n  

# LOCALIZATION NOTE (seamonkey_l10n_long):
# insert full bookmark line for localized SeaMonkey page (normal bookmark)
# e.g. #define seamonkey_l10n_long <DT><A HREF="https://www.seamonkey.tld/">tlhIngan Hol SeaMonkey</a>
# Do not remove the trailing spaces here or or MERGE_FILE in l0n builds will fail!
#define seamonkey_l10n_long  

#unfilter emptyLines
