<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!ENTITY itemGeneral.label       "ზოგადი">
<!ENTITY itemDisplay.label       "წაკითხვა და ჩვენება">
<!ENTITY itemUpdate.label        "განახლება">
<!ENTITY itemNetworking.label    "ქსელი და ადგილი დისკზე">
<!ENTITY itemCertificates.label  "სერთიფიკატები">

<!-- General Settings -->

<!ENTITY submitTelemetry.label         "წარმადობის მონაცემთა გაგზავნა">
<!ENTITY submitTelemetry.accesskey     "P">
<!ENTITY telemetryInfoLink.label       "ვრცლად ტელემეტრიის შესახებ">
<!ENTITY enableGlodaSearch.label             "გლობალური ძიებისა და გზავნილთა ინდექსაციის ჩართვა">
<!ENTITY enableGlodaSearch.accesskey   "E">

<!ENTITY scrolling.label               "გადახვევა">
<!ENTITY useAutoScroll.label           "თვითგადახვევა">
<!ENTITY useAutoScroll.accesskey       "U">
<!ENTITY useSmoothScrolling.label      "მდორე გადახვევა">
<!ENTITY useSmoothScrolling.accesskey  "m">

<!ENTITY systemIntegration.label       "სისტემური ინტეგრაცია">
<!ENTITY alwaysCheckDefault.label      "გახსნისას &brandShortName; პროგრამის ყოველთვის შემოწმება ნაგულისხმებ საფოსტო პროგრამად">
<!ENTITY alwaysCheckDefault.accesskey  "A">
<!ENTITY searchIntegration.label       "&searchIntegration.engineName; სისტემისთვის წერილებში ძიების ნებართვა">
<!ENTITY searchIntegration.accesskey   "S">
<!ENTITY checkDefaultsNow.label        "შემოწმება ახლავე…">
<!ENTITY checkDefaultsNow.accesskey    "N">
<!ENTITY configEditDesc.label          "დეტალური გამართვა">
<!ENTITY configEdit.label              "გამართვის რედაქტორი…">
<!ENTITY configEdit.accesskey          "C">
<!ENTITY returnReceiptsInfo.label      "როგორ მოეპყრას &brandShortName; მიღების დასტურებს">
<!ENTITY showReturnReceipts.label      "მიღების დასტურები…">
<!ENTITY showReturnReceipts.accesskey  "R">

<!-- Display and Reading Settings -->
<!ENTITY reading.caption               "წაკითხვა">
<!ENTITY display.caption               "ჩვენება">
<!ENTITY showCondensedAddresses.label  "წიგნაკში შეტანილი პირების მხოლოდ ეკრანული სახელის ჩვენება">
<!ENTITY showCondensedAddresses.accesskey "S">

<!ENTITY autoMarkAsRead.label          "შეტყობინებების წაკითხულად ავტომატურად მონიშვნა">
<!ENTITY autoMarkAsRead.accesskey      "A">
<!ENTITY markAsReadNoDelay.label       "დაუყოვნებლივ ჩვენების დაწყებისას">
<!ENTITY markAsReadNoDelay.accesskey   "o">
<!-- LOCALIZATION NOTE (markAsReadDelay.label): This will concatenate to
     "After displaying for [___] seconds",
     using (markAsReadDelay.label) and a number (secondsLabel.label). -->
<!ENTITY markAsReadDelay.label         "დაცდა წერილის წაკითხულად მონიშვნამდე">
<!ENTITY markAsReadDelay.accesskey     "d">
<!ENTITY secondsLabel.label            "წამი">
<!ENTITY openMsgIn.label                "წერილის გახსნა აქ:">
<!ENTITY openMsgInNewTab.label         "ახალი დაფა">
<!ENTITY openMsgInNewTab.accesskey     "t">
<!ENTITY reuseExpRadio0.label          "წერილის ახალ ფანჯარაში">
<!ENTITY reuseExpRadio0.accesskey      "n">
<!ENTITY reuseExpRadio1.label          "წერილის არსებულ ფანჯარაში">
<!ENTITY reuseExpRadio1.accesskey      "e">
<!ENTITY closeMsgOnMoveOrDelete.label  "წერილის სარკმლის დახურვა წაშლისას">
<!ENTITY closeMsgOnMoveOrDelete.accesskey "C">

<!-- Update -->
<!ENTITY autoCheck.label                "განახლებების ავტომატური შემოწმება:">
<!ENTITY enableAppUpdate.label          "&brandShortName;">
<!ENTITY enableAppUpdate.accesskey      "h">
<!ENTITY enableAddonsUpdate.label       "ჩადგმული პაკეტები და თემები">
<!ENTITY enableAddonsUpdate.accesskey   "A">
<!ENTITY whenUpdatesFound.label         "&brandShortName; განახლების მოძებნის შემთხვევაში,">
<!ENTITY modeAskMe.label                "შემდეგ ნაბიჯის დაზუსტება">
<!ENTITY modeAskMe.accesskey            "m">
<!ENTITY modeAutomatic.label            "განახლების ავტომატური ჩამოტვირთვა და ჩადგმა">
<!ENTITY modeAutomatic.accesskey        "d">
<!ENTITY modeAutoAddonWarn.label        "გაფრთხილება თუ გამოიწვევს სხვა პაკეტების ან თემების გაუქმებას">
<!ENTITY modeAutoAddonWarn.accesskey    "W">
<!ENTITY showUpdates.label              "განახლების ისტორიის ჩვენება">
<!ENTITY showUpdates.accesskey          "S">

<!ENTITY useService.label                "ფონური მოსახურებით სარგებლობა განახლებათა ჩადგმისას">
<!ENTITY useService.accesskey            "b">

<!-- Networking and Disk Space -->
<!ENTITY showSettings.label            "პარამეტრები…">
<!ENTITY showSettings.accesskey        "S">
<!ENTITY proxiesConfigure.label        "როგორ დაუკავშირდეს &brandShortName; პროგრამა ინტერნეტს.">
<!ENTITY connectionsInfo.caption       "კავშირი">
<!ENTITY offlineInfo.caption           "ავტონომიური რეჟიმი">
<!ENTITY offlineInfo.label             "ავტონომიური რეჟიმის პარამეტრები">
<!ENTITY showOffline.label             "ავტონომიური რეჟიმი…">
<!ENTITY showOffline.accesskey         "O">

<!ENTITY Diskspace "ადგილი დისკზე">
<!ENTITY offlineCompactFolders.label "დასტების შემჭიდროება თუ გამოთავისუფლდება">
<!ENTITY offlineCompactFolders.accesskey "a">
<!ENTITY offlineCompactFoldersMB.label "მბ მაინც">

<!-- LOCALIZATION NOTE:
  The entities useCacheBefore.label and useCacheAfter.label appear on a single
  line in preferences as follows:

  &useCacheBefore.label  [ textbox for cache size in MB ]   &useCacheAfter.label;
-->
<!ENTITY useCacheBefore.label            "ბუფერის ზომა">
<!ENTITY useCacheBefore.accesskey        "U">
<!ENTITY useCacheAfter.label             "მბ ბუფერისთვის">
<!ENTITY clearCacheNow.label             "გასუფთავება">
<!ENTITY clearCacheNow.accesskey         "C">

<!-- Certificates -->
<!ENTITY certSelection.description       "როცა სერვერი ჩემს პირად სერთიფიკატს ითხოვს:">
<!ENTITY certs.auto                      "ავტომატური შერჩევა">
<!ENTITY certs.auto.accesskey            "m">
<!ENTITY certs.ask                       "დამეკითხე ყოველთვის">
<!ENTITY certs.ask.accesskey             "A">

<!ENTITY viewCertificates.label         "სერთიფიკატების ჩვენება">
<!ENTITY viewCertificates.accesskey     "C">
<!ENTITY validation.label               "შემოწმება">
<!ENTITY validation.accesskey           "V">
<!ENTITY viewSecurityDevices.label      "უსაფრთხოების მოწყობილობები">
<!ENTITY viewSecurityDevices.accesskey  "S">
