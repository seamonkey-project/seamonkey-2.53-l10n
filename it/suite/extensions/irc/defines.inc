#filter emptyLines

#define MOZ_LANGPACK_CREATOR Il team ChatZilla

#define MOZ_LANGPACK_HOMEPAGE http://chatzilla.hacksrus.com/

# If non-English locales wish to credit multiple contributors, uncomment this
# variable definition and use the format specified.
#define MOZ_LANGPACK_CONTRIBUTORS <em:contributor>Associazione Italiana Supporto e Traduzione Mozilla</em:contributor> <em:contributor>Iacopo Benesperi</em:contributor> <em:contributor>Francesco Lodolo</em:contributor> <em:contributor>Giacomo Magnini</em:contributor>

#unfilter emptyLines
