<!-- This Source Code Form is subject to the terms of the Mozilla Public
   - License, v. 2.0. If a copy of the MPL was not distributed with this
   - file, You can obtain one at http://mozilla.org/MPL/2.0/. -->

<!-- Title -->
<!ENTITY addressbookWindow.title "Książka adresowa">
<!ENTITY blankResultsPaneMessage.label "Ta książka adresowa wyświetla kontakty wyłącznie po wyszukiwaniu">
<!ENTITY localResultsOnlyMessage.label "Kontakty ze zdalnych książek adresowych nie są wyświetlane, zanim nie zostaną wyszukane">

<!-- File Menu -->
<!ENTITY newContact.label "Wizytówka…">
<!ENTITY newContact.accesskey "W">
<!ENTITY newContact.key "N">
<!ENTITY newListCmd.label "Lista dystrybucyjna…">
<!ENTITY newListCmd.accesskey "l">
<!ENTITY newAddressBookCmd.label "Książka adresowa…">
<!ENTITY newAddressBookCmd.accesskey "k">
<!ENTITY newLDAPDirectoryCmd.label "Katalog LDAP…">
<!ENTITY newLDAPDirectoryCmd.accesskey "D">
<!ENTITY printContactViewCmd.label "Drukuj wizytówkę…">
<!ENTITY printContactViewCmd.accesskey "D">
<!ENTITY printContactViewCmd.key "P">
<!ENTITY printPreviewContactViewCmd.label "Podgląd wydruku wizytówki">
<!ENTITY printPreviewContactViewCmd.accesskey "w">
<!ENTITY printAddressBook.label "Drukuj książkę adresową…">
<!ENTITY printAddressBook.accesskey "a">
<!ENTITY printPreviewAddressBook.label "Podgląd wydruku książki adresowej">
<!ENTITY printPreviewAddressBook.accesskey "P">

<!-- Edit Menu -->
<!ENTITY deleteAbCmd.label "Usuń książkę adresową">
<!ENTITY deleteContactCmd.label "Usuń wizytówkę">
<!ENTITY deleteContactsCmd.label "Usuń zaznaczone wizytówki">
<!ENTITY deleteListCmd.label "Usuń listę">
<!ENTITY deleteListsCmd.label "Usuń zaznaczone listy">
<!ENTITY deleteItemsCmd.label "Usuń zaznaczone elementy">
<!ENTITY swapFirstNameLastNameCmd.label "Zamień miejscami Imię/Nazwisko">
<!ENTITY swapFirstNameLastNameCmd.accesskey "w">
<!ENTITY propertiesCmd.label "Właściwości…">
<!ENTITY propertiesCmd.accesskey "i">
<!ENTITY propertiesCmd.key "i">

<!-- View Menu -->
<!ENTITY showAbToolbarCmd.label "Pasek narzędzi książki adresowej">
<!ENTITY showAbToolbarCmd.accesskey "o">
<!ENTITY layoutMenu.label "Układ">
<!ENTITY layoutMenu.accesskey "U">
<!ENTITY showDirectoryPane.label "Panel książek adresowych">
<!ENTITY showDirectoryPane.accesskey "P">
<!ENTITY showContactPane2.label "Panel kontaktu">
<!ENTITY showContactPane2.accesskey "k">
<!ENTITY menu_ShowNameAs.label "Wyświetlaj nazwę jako">
<!ENTITY menu_ShowNameAs.accesskey "z">
<!ENTITY firstLastCmd.label "Imię Nazwisko">
<!ENTITY firstLastCmd.accesskey "I">
<!ENTITY lastFirstCmd.label "Nazwisko, imię">
<!ENTITY lastFirstCmd.accesskey "N">
<!ENTITY displayNameCmd.label "Wyświetlana nazwa">
<!ENTITY displayNameCmd.accesskey "W">
<!-- LOCALIZATION NOTE (toggleDirectoryPaneCmd.key): This is only used on the
     mac platform, other platforms use VK_F9. -->
<!ENTITY toggleDirectoryPaneCmd.key "S">

<!-- Tasks Menu -->
<!ENTITY importCmd.label "Importuj…">
<!ENTITY importCmd.accesskey "I">
<!ENTITY exportCmd.label "Eksportuj…">
<!ENTITY exportCmd.accesskey "E">

<!-- Toolbar and Popup items -->  
<!ENTITY newContactButton.label "Nowa wizytówka">
<!ENTITY newContactButton.accesskey "N">
<!ENTITY newlistButton.label "Nowa lista dystrybucyjna">
<!ENTITY newlistButton.accesskey "l">
<!ENTITY editItemButton.label "Właściwości">
<!ENTITY editItemButton.accesskey "W">
<!ENTITY newmsgButton.label "Nowa">
<!ENTITY newmsgButton.accesskey "N">
<!ENTITY deleteItemButton.label "Usuń">
<!ENTITY deleteItemButton.accesskey "U">
<!ENTITY printButton.label "Drukuj">
<!ENTITY printButton.accesskey "D">
<!ENTITY searchNameAndEmail.placeholder "Szukaj w nazwiskach, imionach oraz adresach e-mail">
<!ENTITY searchBox.title "Szukaj">

<!-- Tooltips -->
<!ENTITY addressbookToolbar.tooltip "Pasek narzędzi książki adresowej">
<!ENTITY newContactButton.tooltip "Dodaj nową wizytówkę do książki adresowej">
<!ENTITY newlistButton.tooltip "Utwórz nową listę dystrybucyjną">
<!ENTITY editItemButton.tooltip "Edytuj zaznaczony element">
<!ENTITY newmsgButton.tooltip "Wyślij wiadomość">
<!ENTITY printButton.tooltip "Wydrukuj zaznaczony element">
<!ENTITY deleteItemButton.tooltip "Usuń zaznaczony element">
<!ENTITY advancedButton.tooltip "Zaawansowane wyszukiwanie adresów">

<!-- Dir Tree header -->
<!ENTITY dirTreeHeader.label "Książki adresowe">

<!-- Card Summary Pane -->
<!-- Box Headings -->
<!ENTITY contact.heading "Kontakt">
<!ENTITY home.heading "Dom">
<!ENTITY other.heading "Inne">
<!ENTITY chat.heading "Komunikator">
<!ENTITY phone.heading "Telefon">
<!ENTITY work.heading "Praca">
<!-- Special Box Headings, for mailing lists -->
<!ENTITY description.heading "Opis">
<!ENTITY addresses.heading "Adresy">
<!-- For Map It! -->
<!ENTITY mapItButton.label "Pokaż mapę">
<!ENTITY mapIt.tooltip "Wyświetl mapę z sieci WWW dla tego adresu">

<!-- Status Bar -->  
<!ENTITY statusText.label "">

<!-- LOCALIZATION NOTE (hideSwapFnLnUI) : DONT_TRANSLATE -->
<!-- Swap FN/LN UI  Set to "false" to show swap fn/ln UI -->
<!ENTITY hideSwapFnLnUI "true">
