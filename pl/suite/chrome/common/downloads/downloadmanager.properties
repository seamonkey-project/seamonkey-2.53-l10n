# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (stateUnknown):
# Indicates that the download stat is unknown.
# You should never see this in the ui.
stateUnknown=Niewiadoma
# LOCALIZATION NOTE (stateDownloading):
# Indicates that the download is in progress.
stateDownloading=Pobieranie
# LOCALIZATION NOTE (stateUploading):
# Indicates that the upload is in progress.
stateUploading=Wysyłanie
# LOCALIZATION NOTE (stateStarting):
# Indicates that the download is starting.
# You won't probably ever see this in the ui.
stateStarting=Rozpoczynanie…
# LOCALIZATION NOTE (stateNotStarted):
# Indicates that the download has not started yet.
# You won't probably ever see this in the ui.
stateNotStarted=Nie rozpoczęto
# LOCALIZATION NOTE (stateScanning):
# Indicates that an external program is scanning the download for viruses.
stateScanning=Skanowanie w poszukiwaniu wirusów…
# LOCALIZATION NOTE (stateFailed):
# Indicates that the download failed because of an error.
stateFailed=Pobieranie nie powiodło się
# LOCALIZATION NOTE (statePaused):
# Indicates that the download was paused by the user.
statePaused=Pobieranie wstrzymane
# LOCALIZATION NOTE (stateCanceled):
# Indicates that the download was canceled by the user.
stateCanceled=Pobieranie anulowane
# LOCALIZATION NOTE (stateCompleted):
# Indicates that the download was completed.
stateCompleted=Pobieranie ukończone
# LOCALIZATION NOTE (stateBlockedParentalControls):
# Indicates that the download was blocked by the Parental Controls feature of
# Windows.  "Parental Controls" should be consistently named and capitalized
# with the display of this feature in Windows.  The following article can
# provide a reference for the translation of "Parental Controls" in various
# languages:
# http://windows.microsoft.com/en-US/windows-vista/Set-up-Parental-Controls
stateBlockedParentalControls=Zablokowane przez kontrolę rodzicielską
# LOCALIZATION NOTE (stateBlockedPolicy):
# Indicates that the download was blocked on Windows because of the "Launching
# applications and unsafe files" setting of the "security zone" associated with
# the target site.  "Security zone" should be consistently named and capitalized
# with the display of this feature in Windows.  The following article can
# provide a reference for the translation of "security zone" in various
# languages:
# http://support.microsoft.com/kb/174360
stateBlockedPolicy=Zablokowane przez politykę stref zabezpieczeń
# LOCALIZATION NOTE (stateDirty):
# Indicates that the download was blocked after scanning.
stateDirty=Zablokowane: plik może zawierać wirusa lub oprogramowanie szpiegujące

blockedMalware=Ten plik zawiera wirusa lub inne złośliwe oprogramowanie
blockedPotentiallyUnwanted=Ten plik może wyrządzić szkody na komputerze
blockedUncommon2=Ten plik nie jest zazwyczaj pobierany

unblockHeaderUnblock=Czy na pewno pozwolić na pobranie tego pliku?
unblockHeaderOpen=Czy na pewno otworzyć ten plik?
unblockTypeMalware=Ten plik zawiera wirusa lub inne złośliwe oprogramowanie, które może wyrządzić szkody na komputerze.
unblockTypePotentiallyUnwanted2=Ten plik, przedstawiany jako pomocny, może wprowadzić nieoczekiwane zmiany w programach i ustawieniach.
unblockTypeUncommon2=Ten plik nie jest zazwyczaj pobierany, a jego otwarcie może nie być bezpieczne. Może zawierać wirusa lub wprowadzić nieoczekiwane zmiany w programach i ustawieniach.
unblockTip2=Można spróbować pobrać plik później lub z innej lokalizacji.
unblockButtonOpen=Otwórz
unblockButtonUnblock=Pozwól
unblockButtonConfirmBlock=Usuń

# LOCALIZATION NOTE (downloadsTitleFiles, downloadsTitlePercent): Semi-colon list of
# plural forms. See: http://developer.mozilla.org/en/Localization_and_Plurals
# %1$S number of files; %2$S overall download percent (only for downloadsTitlePercent)
# %% will appear as a single % sign, so %2$S%% is the percent number plus the % sign
# examples: 2% of 1 file - Download Manager; 22% of 11 files - Download Manager
downloadsTitleFiles=%1$S plik - Pobieranie plików;%1$S pliki - Pobieranie plików;%1$S plików - Pobieranie plików
downloadsTitlePercent=%2$S%% z %1$S pliku - Pobieranie plików;%2$S%% z %1$S plików - Pobieranie plików;%2$S%% z %1$S plików - Pobieranie plików

# LOCALIZATION NOTE (progressTitle):
# %1$S is the file name, %2$S is the download state
# examples: coolvideo.ogg - Finished; seamonkey-nightly.zip - Paused
progressTitle=%1$S - %2$S
# LOCALIZATION NOTE (progressTitlePercent):
# %1$S is download percent, %2$S is the file name, %3$S is the download state
# %% will appear as a single % sign, so %1$S%% is the percent number plus the % sign
# examples: 42% of coolvideo.ogg - Paused; 98% of seamonkey-nightly.zip - Downloading
progressTitlePercent=%1$S%% z %2$S - %3$S

# LOCALIZATION NOTE (percentFormat): %1$S is download percent
# %% will appear as a single % sign, so %1$S%% is the percent number plus the % sign
percentFormat=%1$S%%

# LOCALIZATION NOTE (speedFormat):
# %1$S rate number; %2$S rate unit
# units are taken from toolkit's downloads.properties
# example: 2.2 MB/sec
speedFormat=%1$S %2$S/sek

# LOCALIZATION NOTE (timeSingle): %1$S time number; %2$S time unit
# example: 1 minute; 11 hours
timeSingle=%1$S %2$S
# LOCALIZATION NOTE (timeDouble):
# %1$S time number; %2$S time unit; %3$S time sub number; %4$S time sub unit
# example: 11 hours, 2 minutes; 1 day, 22 hours
timeDouble=%1$S %2$S, %3$S %4$S

# LOCALIZATION NOTE (sizeSpeed):
# %1$S is transfer progress; %2$S download speed
# example: 1.1 of 11.1 GB (2.2 MB/sec)
sizeSpeed=%1$S (%2$S)

# LOCALIZATION NOTE (statusActive): — is the "em dash" (long dash)
# %1$S download status; %2$S time remaining
# example: Paused — 11 hours, 2 minutes remaining
statusActive=%1$S — %2$S

fromSource=Z %S
toTarget=Do %S

otherDownloads3=Jeden inny pobierany plik;Pozostałe %1$S pobierane pliki;Pozostałych %1$S pobieranych plików

fileExecutableSecurityWarning=Uwaga: Rozpoczęto uruchamianie pliku wykonywalnego „%S”. Pliki wykonywalne mogą zawierać wirusy lub inny niebezpieczny kod, który może uszkodzić komputer. Zaleca się zachowanie ostrożności przy otwieraniu plików tego typu. Czy na pewno uruchomić „%S”?
fileExecutableSecurityWarningTitle=Uruchamianie pliku wykonywalnego!
fileExecutableSecurityWarningDontAsk=Nie pytaj ponownie
