#filter emptyLines

# LOCALIZATION NOTE: The 'en-US' strings in some URLs will be replaced with
# your locale code, and link to your translated pages as soon as they're live.

#define bookmarks_title Zakładki
#define bookmarks_heading Zakładki

#define personal_toolbarfolder Folder zakładek osobistych

# LOCALIZATION NOTE (seamonkey):
# link title for https://www.seamonkey-project.org/ (in the personal toolbar)
#define seamonkey Projekt SeaMonkey

# LOCALIZATION NOTE (seamonkey_long):
# link title for https://www.seamonkey-project.org/ (in normal bookmarks)
#define seamonkey_long Projekt SeaMonkey (witryna w jęz. angielskim)

#define extend_seamonkey Rozszerzanie programu SeaMonkey

# LOCALIZATION NOTE (seamonkey_addons):
# link title for https://addons.thunderbird.net/en-US/seamonkey/
#define seamonkey_addons Dodatki dla programu SeaMonkey

# LOCALIZATION NOTE (seamonkey_themes):
# link title for https://addons.thunderbird.net/en-US/seamonkey/themes
#define seamonkey_themes Motywy dla programu SeaMonkey

# LOCALIZATION NOTE (seamonkey_dictionaries):
# link title for https://addons.thunderbird.net/en-US/seamonkey/dictionaries
#define seamonkey_dictionaries Słowniki do sprawdzania pisowni

#define community_support Społeczność i pomoc

# LOCALIZATION NOTE (seamonkey_community):
# link title for https://www.seamonkey-project.org/community
#define seamonkey_community Społeczność wokół programu SeaMonkey

# LOCALIZATION NOTE (mozillazine):
# link title for http://www.mozillazine.org/
#define mozillazine MozillaZine

# LOCALIZATION NOTE (seamonkey_support):
# link title for the mozillaZine SeaMonkey Support forum
#define seamonkey_support Pomoc dla SeaMonkey na forum witryny MozillaZine

# LOCALIZATION NOTE (seamonkey_l10n):
# insert full bookmark line for localized SeaMonkey page (personal toolbar)
# e.g. #define seamonkey_l10n <DT><A HREF="https://www.seamonkey.tlh/">SeaMonkey tlhIngan</a>
#define seamonkey_l10n <DT><A HREF="https://www.seamonkey.pl/">SeaMonkey.pl</a>

# LOCALIZATION NOTE (seamonkey_l10n_long):
# insert full bookmark line for localized SeaMonkey page (normal bookmark)
# e.g. #define seamonkey_l10n <DT><A HREF="https://www.seamonkey.tld/">tlhIngan Hol SeaMonkey</a>
#define seamonkey_l10n_long <DT><A HREF="https://www.seamonkey.pl/">SeaMonkey.pl</a>

#unfilter emptyLines
