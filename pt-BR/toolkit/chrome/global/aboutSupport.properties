# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE (crashesTitle): Semi-colon list of plural forms.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
# #1 number of relevant days with crash reports
crashesTitle=Relatórios de travamentos de #1 dia;Relatórios de travamentos dos últimos #1 dias

# LOCALIZATION NOTE (crashesTimeMinutes): Semi-colon list of plural forms.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
# #1 number of minutes (between 1 and 59) which have passed since the crash
crashesTimeMinutes=Há #1 minuto;Há #1 minutos

# LOCALIZATION NOTE (crashesTimeHours): Semi-colon list of plural forms.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
# #1 number of hours (between 1 and 23) which have passed since the crash
crashesTimeHours=Há #1 hora;Há #1 horas

# LOCALIZATION NOTE (crashesTimeDays): Semi-colon list of plural forms.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
# #1 number of days (1 or more) which have passed since the crash
crashesTimeDays=Há #1 dia;Há #1 dias

# LOCALIZATION NOTE (pendingReports): Semi-colon list of plural forms.
# See: http://developer.mozilla.org/en/docs/Localization_and_Plurals
# #1 number of pending crash reports
pendingReports=Todos os relatórios de travamentos (incluindo #1 travamento pendente na faixa de tempo especificada);Todos os relatórios de travamentos (incluindo #1 travamentos pendentes na faixa de tempo especificada)

# LOCALIZATION NOTE (rawDataCopied) Text displayed in a mobile "Toast" to user when the
# raw data is successfully copied to the clipboard via button press.
rawDataCopied=Dados copiados para a área de transferência

# LOCALIZATION NOTE (textCopied) Text displayed in a mobile "Toast" to user when the
# text is successfully copied to the clipboard via button press.
textCopied=Texto copiado para a área de transferência

# LOCALIZATION NOTE The verb "blocked" here refers to a graphics feature such as "Direct2D" or "OpenGL layers".
blockedDriver = Bloqueado para a versão do seu driver gráfico.

# LOCALIZATION NOTE The %S here is a placeholder, leave unchanged, it will get replaced by the driver version string.
tryNewerDriver = Bloqueado para a versão do seu driver gráfico. Tentar atualizá-lo para, pelo menos, a versão %S.

# LOCALIZATION NOTE The verb "blocked" here refers to a graphics feature such as "Direct2D" or "OpenGL layers".
blockedGfxCard = Bloqueado para sua placa gráfica devido a problemas de driver não resolvidos.

# LOCALIZATION NOTE The verb "blocked" here refers to a graphics feature such as "Direct2D" or "OpenGL layers".
blockedOSVersion = Bloqueado para a versão do seu sistema operacional.

# LOCALIZATION NOTE The verb "blocked" here refers to a graphics feature such as "Direct2D" or "OpenGL layers".
blockedMismatchedVersion = Bloqueado para a sua versão incompatível do driver de video no registro e DLL

# LOCALIZATION NOTE In the following strings, "Direct2D", "DirectWrite" and "ClearType"
# are proper nouns and should not be translated. Feel free to leave english strings if
# there are no good translations, these are only used in about:support
clearTypeParameters = Parâmetros ClearType

compositing = Composição
hardwareH264 = Decodificação H264 por hardware
mainThreadNoOMTC = tópico principal, sem OMTC
yes = Sim
no = Não
# LOCALIZATION NOTE The following strings indicate if an API key has been found.
# In some development versions, it's expected for some API keys that they are
# not found.
found = Encontrado
missing = Faltando

# LOCALIZATION NOTE (dataSize):
#   This is the amount data being stored, where we insert storage size and unit.
#   e.g., "Disk Space Available 200 GB"
#   %1$S = size
#   %2$S = unit (MB, KB, etc.)
dataSize=%1$S %2$S

gpuDescription = Descrição
gpuVendorID = ID do fornecedor
gpuDeviceID = ID do dispositivo
gpuSubsysID = ID do subsistema
gpuDrivers = Drivers
gpuRAM = RAM
gpuDriverVersion = Versão do driver
gpuDriverDate = Data do driver
gpuActive = Ativo
webgl1WSIInfo = Info WSI do driver WebGL 1
webgl1Renderer = Renderizador do driver WebGL 1
webgl1Version = Versão do driver WebGL 1
webgl1DriverExtensions = Extensões do driver WebGL 1
webgl1Extensions = Extensões WebGL 1
webgl2WSIInfo = Info WSI do driver WebGL 2
webgl2Renderer = Renderizador do driver WebGL 2
webgl2Version = Versão do driver WebGL 2
webgl2DriverExtensions = Extensões do driver WebGL 2
webgl2Extensions = Extensões WebGL 2
GPU1 = GPU #1
GPU2 = GPU #2
blocklistedBug = Bloqueado devido a problemas conhecidos
# LOCALIZATION NOTE %1$S will be replaced with a bug number string.
bugLink = bug %1$S
# LOCALIZATION NOTE %1$S will be replaced with an arbitrary identifier
# string that can be searched on DXR/MXR or grepped in the source tree.
unknownFailure = Bloqueado; código de erro %1$S
d3d11layersCrashGuard = Compositor D3D11
d3d11videoCrashGuard = Decodificador de vídeo D3D11
d3d9videoCrashGuard = Decodificador de vídeo D3D9
glcontextCrashGuard = OpenGL
resetOnNextRestart = Redefinir na próxima reinicialização
gpuProcessKillButton = Finalizar processo GPU
gpuDeviceResetButton = Ativar a Redefinição de Dispositivo
usesTiling = Usa mosaicos
offMainThreadPaintEnabled = Ativado o desenho fora do processo principal
offMainThreadPaintWorkerCount = Contagem de desenho fora do tópico principal

audioBackend = Infraestrutura de Áudio
maxAudioChannels = Máximo de Canais
channelLayout = Formato de Canal Preferido
sampleRate = Taxa de amostragem preferida

minLibVersions = Versão mínima esperada
loadedLibVersions = Versão em uso

hasSeccompBPF = Seccomp-BPF (Sistema de filtragem de chamadas)
hasSeccompTSync = Sincronização do Tópico Seccomp
hasUserNamespaces = Espaço de nomes do usuário
hasPrivilegedUserNamespaces = Espaço de nomes do usuário para processos privilegiados
canSandboxContent = Isolamento de processamento de conteúdo
canSandboxMedia = Isolamento de plugins de mídia
contentSandboxLevel = Nível de isolamento de processamento de conteúdo
effectiveContentSandboxLevel = Nível efetivo de isolamento de processamento de conteúdo
sandboxProcType.content = conteúdo
sandboxProcType.file = conteúdo do arquivo
sandboxProcType.mediaPlugin = plugin de mídia

# LOCALIZATION NOTE %1$S and %2$S will be replaced with the number of remote and the total number
# of windows, respectively, while %3$S will be replaced with one of the status strings below,
# which contains a description of the multi-process preference and status.
# Note: multiProcessStatus.3 doesn't exist because status=3 was deprecated.
multiProcessWindows = %1$S/%2$S (%3$S)
multiProcessStatus.0 = Ativado pelo usuário
multiProcessStatus.1 = Ativado por padrão
multiProcessStatus.2 = Desativado
multiProcessStatus.4 = Desativado por ferramentas de acessibilidade
multiProcessStatus.5 = Desativado por falta de aceleração de gráficos por hardware no Mac OS X
multiProcessStatus.6 = Desativado por entrada de texto não suportada
multiProcessStatus.7 = Desativado por extensões
multiProcessStatus.8 = Desativado à força
multiProcessStatus.unknown = Status desconhecido

asyncPanZoom = Deslocamento/Zoom assíncrono
apzNone = nenhum
wheelEnabled = entrada com roda do mouse ativada
touchEnabled = entrada touch ativada
dragEnabled = arrasto da barra de rolagem ativado
keyboardEnabled = teclado ativado
autoscrollEnabled = rolagem automática ativada

# LOCALIZATION NOTE %1 will be replaced with the key of a preference.
wheelWarning = entrada com roda do mouse assíncrona desativada devido a preferência não suportada: %S
touchWarning = entrada touch assíncrona desativada devido a preferência não suportada: %S

# LOCALIZATION NOTE Strings explaining why a feature is or is not available.
disabledByBuild = desabilitado pela compilação
enabledByDefault = habilitado por padrão
disabledByDefault = desabilitado por padrão
enabledByUser = habilitado pelo usuário
disabledByUser = desabilitado pelo usuário
